UPDATE jp_team_person
SET 
	   <#if jpTeamPerson.createName ?exists>
		   create_name = :jpTeamPerson.createName,
		</#if>
	   <#if jpTeamPerson.createBy ?exists>
		   create_by = :jpTeamPerson.createBy,
		</#if>
	    <#if jpTeamPerson.createDate ?exists>
		   create_date = :jpTeamPerson.createDate,
		</#if>
	   <#if jpTeamPerson.updateName ?exists>
		   update_name = :jpTeamPerson.updateName,
		</#if>
	   <#if jpTeamPerson.updateBy ?exists>
		   update_by = :jpTeamPerson.updateBy,
		</#if>
	    <#if jpTeamPerson.updateDate ?exists>
		   update_date = :jpTeamPerson.updateDate,
		</#if>
	   <#if jpTeamPerson.sysOrgCode ?exists>
		   sys_org_code = :jpTeamPerson.sysOrgCode,
		</#if>
	   <#if jpTeamPerson.sysCompanyCode ?exists>
		   sys_company_code = :jpTeamPerson.sysCompanyCode,
		</#if>
	   <#if jpTeamPerson.name ?exists>
		   name = :jpTeamPerson.name,
		</#if>
	   <#if jpTeamPerson.imgSrc ?exists>
		   img_src = :jpTeamPerson.imgSrc,
		</#if>
	   <#if jpTeamPerson.introduction ?exists>
		   introduction = :jpTeamPerson.introduction,
		</#if>
	    <#if jpTeamPerson.jionDate ?exists>
		   jion_date = :jpTeamPerson.jionDate,
		</#if>
	   <#if jpTeamPerson.isJoin ?exists>
		   is_join = :jpTeamPerson.isJoin,
		</#if>
WHERE id = :jpTeamPerson.id